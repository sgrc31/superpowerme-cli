import os

import peewee as pw
from dotenv import load_dotenv


load_dotenv()

my_db = pw.MySQLDatabase(
    os.environ.get("REMOTEDBNAME"),
    host=os.environ.get("REMOTEDBHOST"),
    user=os.environ.get("REMOTEDBUSER"),
    password=os.environ.get("REMOTEDBPASS"),
)


class BaseModel(pw.Model):
    class Meta:
        database = my_db


class User(BaseModel):
    id = pw.AutoField()
    name = pw.CharField()
    surname = pw.CharField()
    email = pw.CharField()
    created_at = pw.DateTimeField()
    updated_at = pw.DateTimeField()

    class Meta:
        table_name = "users"


class Hero(BaseModel):
    id = pw.AutoField()
    name = pw.CharField()
    sentences = pw.TextField()
    title = pw.CharField()

    class Meta:
        table_name = "heroes"


class Game(BaseModel):
    id = pw.AutoField()
    user_id = pw.ForeignKeyField(User, backref="user_id")
    selected_hero_id = pw.ForeignKeyField(Hero, backref="hero_id")
    coins = pw.IntegerField()
    created_at = pw.DateTimeField()
    updated_at = pw.DateTimeField()

    class Meta:
        table_name = "games"


class Quest(BaseModel):
    id = pw.BigAutoField()
    title = pw.CharField()

    class Meta:
        table_name = "quests"


class GameQuest(BaseModel):
    id = pw.BigAutoField()
    game_id = pw.ForeignKeyField(Game, backref="game_id")
    quest_id = pw.ForeignKeyField(Quest, backref="quest_id")
    completed = pw.BooleanField()
    updated_at = pw.DateTimeField()

    class Meta:
        table_name = "game_quest"
