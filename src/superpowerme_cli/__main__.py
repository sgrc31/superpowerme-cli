#!/usr/bin/env python3
from . import config
from . import core
from .cli import app


def create_patients() -> None:
    for (identifier, data) in config.yaml_dict.items():
        core.Patient(*data)


def main() -> None:
    create_patients()
    app()


if __name__ == "__main__":
    main()
