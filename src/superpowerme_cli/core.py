from __future__ import annotations

import os
from datetime import date
from typing import Optional

import yagmail
from dotenv import load_dotenv
from openpyxl import Workbook

from . import config
from . import db

load_dotenv()

heroes_dict = {
    1: "Beck",
    2: "Greta",
    3: "Jhonny 6",
    4: "Andrea",
}


class Patient:
    """This class rapresents our patien and their data"""

    quests_dict = {}

    patients_dict: dict[str, Patient] = {}

    def __init__(
        self,
        name: str,
        id: int,
        doctor_id: str,
    ) -> None:
        self._name = name
        self._user_id = id
        self._identifier = doctor_id
        self._game_instance: Optional[db.Game] = None
        self._current_hero: Optional[str] = None
        self._coins: Optional[int] = None
        self._completed_quests = None
        self._last_login_at = None
        Patient.patients_dict[self._identifier] = self

    def __repr__(self) -> str:
        return f"{self._identifier} - {self._name}"

    def collect_data(self) -> None:
        self._get_game_instance()
        self._get_current_hero()
        self._get_coins()
        self._get_last_login()

    def set_coins(self, coins_n: int):
        self._game_instance.coins = coins_n
        self._game_instance.save()
        print("monete aggiornate")

    def _get_game_instance(self) -> Optional[db.Game]:
        self._game_instance = db.Game.get(db.Game.user_id == self._user_id)
        return self._game_instance

    def _get_current_hero(self) -> Optional[str]:
        hero_number = self._game_instance.selected_hero_id.id
        self._current_hero = heroes_dict[hero_number]
        return self._current_hero

    def _get_coins(self) -> Optional[int]:
        self._coins = self._game_instance.coins
        return self._coins

    def _get_last_login(self):
        self._last_login_at = self._game_instance.updated_at
        return self._last_login_at

    def _get_completed_quests_list(self):
        pass

    @classmethod
    def get_quests_dict(cls):
        """Returns a dict containing all the quests, in format <quest.id>: <quest.title>"""
        quests_list = [(quest.id, quest.title) for quest in db.Quest.select()]
        quests_dict = {tupla[0]: tupla[-1] for tupla in quests_list}
        return quests_dict

    @classmethod
    def populate_patients_dict(cls) -> None:
        cls.patients_dict = {
            identifier: Patient(*data)
            for (identifier, data) in config.yaml_dict.items()
        }


class Report:
    """This class manages the creation and sending of report files"""

    wb = Workbook()

    def __init__(self, patient: Patient) -> None:
        self.patient = patient
        self.wb_sheet = None
        self._generate_sheet()
        self._insert_header()
        self._populate_header()

    def _generate_sheet(self) -> None:
        Report.wb.create_sheet(repr(self.patient))
        self.wb_sheet = Report.wb.get_sheet_by_name(repr(self.patient))

    def _insert_header(self) -> None:
        self.wb_sheet["A1"] = "Nome Paziente"
        self.wb_sheet["A2"] = "Identificativo"
        self.wb_sheet["A3"] = "Monete"
        self.wb_sheet["A4"] = "Ultimo login"
        self.wb_sheet["A5"] = "Numero missioni svolte"
        self.wb_sheet["A6"] = "Nuova missione iniziata"
        self.wb_sheet["A8"] = "Ore maschera"

    def _populate_header(self) -> None:
        self.wb_sheet["B1"] = repr(self.patient)
        self.wb_sheet["B2"] = self.patient._identifier
        self.wb_sheet["B3"] = self.patient._coins
        self.wb_sheet["B4"] = self.patient._last_login_at
        self.wb_sheet["B5"] = self.patient._identifier

    def __repr__(self) -> str:
        return f"report per {self.patient}"

    @classmethod
    def remove_default_sheet(cls) -> Workbook:
        stbr = Report.wb.get_sheet_by_name("Sheet")
        Report.wb.remove_sheet(stbr)
        return Report.wb

    @classmethod
    def save_workbook(cls) -> None:
        generation_date = date.today().strftime("%Y%m%d")
        Report.wb.save(f"superpowerme-{generation_date}.xlsx")
        print("File generato con successo")

    @classmethod
    def send_report_via_email(cls) -> None:
        generation_date = date.today()
        control_recipient = "s.guercio@gmail.com"
        mail_recipients = [
            "sara.viscione@student.unisi.it",
            "margherita.cini@student.unisi.it",
            "giulia.teverini@student.unisi.it",
        ]
        subject_message = "Report giornaliero SuperPowerMe"
        body_message = f"Report SuperPowerMe generato in data {generation_date}"

        with yagmail.SMTP(
            os.environ.get("GMUSERNAME"), os.environ.get("GMPASSWORD")
        ) as yag:
            yag.send(
                to=control_recipient,
                bcc=mail_recipients,
                subject=subject_message,
                contents=body_message,
            )
