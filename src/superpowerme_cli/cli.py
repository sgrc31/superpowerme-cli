import typer
from rich.console import Console
from rich.table import Table

from . import config
from . import core

app = typer.Typer(
    help="SuperPowerMe CLI to manage day to day operations", add_completion=False
)
console = Console()


@app.command(help="Mostra la lista dei pazienti attualmente in sperimentazione")
def list_users():
    """List al current patients, alongside their db id and doctor id"""
    table = Table(title="Lista pazienti attualmente in sperimentazione")
    table.add_column("ID", justify="right", style="cyan")
    table.add_column("Codice", style="magenta")
    table.add_column("Nome", justify="left", style="green")
    for (id, patient) in core.Patient.patients_dict.items():
        table.add_row(
            str(patient._user_id), str(patient._identifier), str(patient._name)
        )
    console.print(table)


@app.command()
def trova_gioco():
    """trova il numero del gioco"""
    for (id, patient) in core.Patient.patients_dict.items():
        patient._get_game_instance()
        patient._get_current_hero()
        print(patient._current_hero)


@app.command(help="Imposta le monete per un paziente")
def set_money(
    patient_identifier: str = typer.Argument(..., help="identificativo del paziente"),
    money: int = typer.Argument(
        ..., help="Il numero di monete da assegnare (1-254)", min=1, max=254
    ),
):
    """Imposta le monete per un utente"""
    try:
        patient = core.Patient.patients_dict[patient_identifier]
    except KeyError:
        print("Invalid identifier")
        return
    patient._get_game_instance()
    patient.set_coins(money)


@app.command(help="Mostra le info di base riguardanti un paziente")
def show_info(
    patient_identifier: str = typer.Argument(..., help="identificativo del paziente")
):
    try:
        patient = core.Patient.patients_dict[patient_identifier]
    except KeyError:
        print("Invalid identifier")
        return
    patient.collect_data()
    print(f"{patient}, monete: {patient._coins}, login: {patient._last_login_at}")


@app.command(help="Crea file di report completi per tutti gli utenti")
def create_report() -> None:
    for identifier, patient in core.Patient.patients_dict.items():
        patient.collect_data()
        newrep = core.Report(patient)
    epurato = core.Report.remove_default_sheet()
    core.Report.save_workbook()


@app.command()
def esp():
    if not core.Patient.quests_dict:
        core.Patient.quests_dict = core.Patient.get_quests_dict()
        print(core.Patient.quests_dict)


@app.command()
def invia_mail() -> None:
    core.Report.send_report_via_email()
