import sys
from pathlib import Path
from typing import Any
from typing import Dict

import yaml
from loguru import logger
from pydantic import BaseSettings
from pydantic import Field
from pydantic import ValidationError
from pydantic import validator


BASE_DIR = Path(__file__).resolve().parent.parent.parent


# patients_dict: Dict[str, core.Patient] = {
#    identifier: core.Patient(*data) for (identifier, data) in patients_db.items()
# }


class Settings(BaseSettings):
    remode_db_host: str = Field(..., env="REMOTEDBHOST")
    remote_db_user: str = Field(..., env="REMOTEDBUSER")
    remote_db_name: str = Field(..., env="REMOTEDBNAME")
    remote_db_pass: str = Field(..., env="REMOTEDBPASS")
    gmail_username: str = Field(..., env="GMUSERNAME")
    gmail_pass: str = Field(..., env="GMPASSWORD")
    yaml_file: str = "patients_db.yml"

    @validator("yaml_file")
    def file_exists(cls, v):
        if not Path(v).exists():
            # Instead of a FileNotfoundError we raise a ValueError
            # as required by pydantic
            raise ValueError(
                f"impossibile trovare il file {v} in" f" {Path(v).resolve().parent}"
            )
        return v

    @validator("yaml_file")
    def is_valid_yaml(cls, v):
        try:
            with open(v, "r") as stream:
                yaml_as_dict = yaml.safe_load(stream)
            return v
        except yaml.parser.ParserError:
            print("errore di configurazione yaml")
            sys.exit()

    @validator("yaml_file")
    def is_properly_structured(cls, v):
        with open(v, "r") as stream:
            yaml_as_dict = yaml.safe_load(stream)
        for (identifier, data) in yaml_as_dict.items():
            assert isinstance(identifier, str), "l'identifier deve essere una stringa"
            assert len(identifier) == 5, "l'identifier deve essere di 5 caratteri"
            assert isinstance(data, list), "i dati devono essere una lista"
            assert isinstance(data[0], str)
            assert isinstance(data[1], int)
            assert identifier == data[2], "non accettiamo discrepanze"
        return v

    def load_yaml_as_dict(self):
        with open(self.yaml_file, "r") as stream:
            yaml_as_dict = yaml.safe_load(stream)
        return yaml_as_dict

    class Config:
        env_file = ".env"


try:
    settings = Settings()
    yaml_dict = settings.load_yaml_as_dict()
except ValidationError as e:
    print(e)
    sys.exit(1)
