.PHONY: test
test:
	poetry run pytest --cov

.PHONY: clean
clean:
	find src/ tests/ -name "*~" -type f -delete
	find src/ tests/ -name "*.pyc" -type f -delete
	find src/ tests/ -name "__pycache__" -type d -delete

.PHONY: mypy
mypy:
	poetry run mypy src/superpowerme_cli/
